// app.js
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

// Rota principal com a barra de navegação
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

// Rotas para cada operação (carregando o conteúdo dinâmico)
app.get('/soma', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/soma.html'));
});

app.get('/subtracao', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/subtracao.html'));
});

app.get('/multiplicacao', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/multiplicacao.html'));
});

app.get('/divisao', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/divisao.html'));
});

app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});
